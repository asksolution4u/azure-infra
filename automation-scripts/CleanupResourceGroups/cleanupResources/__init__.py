import requests
import logging
from datetime import datetime, timedelta
import azure.functions as func
import os

# credentials 
client_id = os.getenv("CLIENT_ID")
client_secret = os.getenv("CLIENT_SECRET")
subscription_id = os.getenv("SUBSCRIPTION_ID")
tenant_id = os.getenv("TENANT_ID")


def get_access_token():
    url = f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"
    payload=f'grant_type=client_credentials&client_id={client_id}&client_secret={client_secret}&resource=https%3A%2F%2Fmanagement.azure.com%2F'
    headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    try:
        token_data =  response.json()["access_token"]
        return token_data
    except:
        logging.error("Couldn't retreive the access token")
        return False
    


# list all the resource groups 
def list_all_resource_groups(token):
    url = f"https://management.azure.com/subscriptions/{subscription_id}/resourcegroups?api-version=2020-09-01"
    payload={}
    headers = {
    'Authorization': f'Bearer {token}'
    }
    try:
        response = requests.request("GET", url, headers=headers, data=payload)
        return response.json()["value"]
    except:
        logging.error("Unable to fetch the all the resource groups")
        return False



# check for the tags and fetch the [delete-after-days] and [created-on] tags. 
def filter_resource_groups(groups):
    tagged_groups = []
    if groups == []:
        logging.info("This subscription doesn't have resource groups")
        return False

    for group in groups:
        try:
            rg = {}
            rg["created_on"] = group["tags"]["created-on"]
            rg["days_to_live"] = group["tags"]["delete-after-days"]
            rg["name"] = group["name"]
            tagged_groups.append(rg)
        except:
            continue 

    if tagged_groups == []:
        logging.info("No Resource Groups to be deleted")
        return False
    return tagged_groups

        
 
# filter the resource groups to be deleted 
def filter_groups_to_delete(groups):
    groups_to_delete = []
    for group in groups:
        try:
            date_created = datetime.strptime(group["created_on"], "%d-%m-%Y")
            days = int(group["days_to_live"])           
            if (datetime.today() - timedelta(days=days)) > date_created:
                groups_to_delete.append(group["name"])
        except:
            logging.info("tag's format is invalid (created_on, days_to_live)")
    return groups_to_delete
        

def delete_resource_group(access_token, rg_name):

    url = f"https://management.azure.com/subscriptions/{subscription_id}/resourcegroups/{rg_name}?api-version=2021-04-01"
    payload={}
    headers = {
    'Authorization': f'Bearer {access_token}'
    }

    response = requests.request("DELETE", url, headers=headers, data=payload)

    
    if response.status_code == 202:
        logging.info(f"RG: {rg_name} submitted to delete.")
    else:
        logging.error(response.text)


def main(mytimer: func.TimerRequest) -> None:
    access_token = get_access_token()
    if access_token:
        resource_groups = list_all_resource_groups(token=access_token)
    if resource_groups:
        tagged_groups = filter_resource_groups(groups=resource_groups)
    if tagged_groups:
        groups_to_delete = filter_groups_to_delete(groups=tagged_groups)
        for group in groups_to_delete:
            delete_resource_group(access_token=access_token, rg_name=group)

