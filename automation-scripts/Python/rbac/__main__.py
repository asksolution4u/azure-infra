from kubernetes import config
from create_namespaced_role import create_namespaced_role
from create_namespaced_role_binding import create_namespaced_role_binding
from create_cluster_role import create_cluster_role
from create_cluster_role_binding import create_cluster_role_binding
import json
import sys,  os, pdb
config.load_kube_config()

def main(rbac_path):
    with open(rbac_path) as f:
    # Load the data from the file
        data = json.load(f)

    kind=data["kind"]
    if kind=="Role":
        create_namespaced_role(rbac_path)
        print("Namespaced role created")
    elif kind=="RoleBinding":
        create_namespaced_role_binding(rbac_path)
        print("Namespaced role binding created")
    elif kind=="ClusterRole":
        create_cluster_role(rbac_path)
        print("Cluster role created")
    elif kind=="ClusterRoleBinding":
        create_cluster_role_binding(rbac_path)
        print("Cluster role binding created")
if __name__ == "__main__":
    #pdb.set_trace()
    main("./rbac_config/Role.json")
    if len(sys.argv)>1:
        n = len(sys.argv[1])
        files = sys.argv[1][1:n-1]
        files = files.split(',')
        for file in files:
            main("./rbac_config/"+file)
            print(file)
