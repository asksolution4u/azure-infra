from kubernetes import client
import json
def create_cluster_role(rbac_path):
    with open(rbac_path) as f:
    # Load the data from the file
        data = json.load(f)

    role_name=data["metadata"]["name"]
    # Create a v1beta1 API client
    rbac_api = client.RbacAuthorizationV1Api()
    try:
        response=rbac_api.read_cluster_role(name=role_name)
    except client.rest.ApiException as e:
        if e.status == 404:
            print(f"The Cluster Role '{role_name}' does not exist.")
        else:
            print("Error occurred:", e)
    # Define the ClusterRole
    cluster_role = client.V1ClusterRole(
        metadata=client.V1ObjectMeta(name=role_name),
        rules=[
            client.V1PolicyRule(
                api_groups=rule["apiGroups"],
                resources=rule["resources"],
                verbs=rule["verbs"],
            )
            for rule in data["rules"]
        ]
    )
    if response!=None:
        rbac_api.replace_cluster_role(role_name,body=cluster_role)
    else:
        # Create the ClusterRole
        rbac_api.create_cluster_role(cluster_role)

