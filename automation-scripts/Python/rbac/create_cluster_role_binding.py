from kubernetes import client, config
import json
def create_cluster_role_binding(rbac_path):
    with open(rbac_path) as f:
    # Load the data from the file
        data = json.load(f)

    role_name=data["metadata"]["name"]
    role_ref=data["roleRef"]
    # Create a v1beta1 API client
    rbac_api = client.RbacAuthorizationV1Api()
    try:
        response=rbac_api.read_cluster_role_binding(name=role_name)
    except client.rest.ApiException as e:
        if e.status == 404:
            print(f"The Cluster Role  Binding'{role_name}' does not exist.")
        else:
            print("Error occurred:", e)
    # Define the ClusterRoleBinding
    cluster_role_binding = client.V1ClusterRoleBinding(
        metadata=client.V1ObjectMeta(name=role_name),
        role_ref=client.V1RoleRef(
            api_group=role_ref["apiGroup"],
            kind=role_ref["kind"],
            name=role_ref["name"],
        ),
        subjects=[
        client.V1Subject(
            kind=subject["kind"],
            name=subject["name"],
            api_group=subject["apiGroup"],
            namespace=subject["namespace"] if 'namespace' in subject else None
        )
        for subject in data["subjects"]
        ]
    )
    if response!=None:
        rbac_api.replace_cluster_role_binding(role_name,cluster_role_binding)
    else:
        # Create the ClusterRoleBinding
        rbac_api.create_cluster_role_binding(cluster_role_binding)
