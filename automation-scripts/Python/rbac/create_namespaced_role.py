from kubernetes import client
import json

def create_namespaced_role(rbac_path):
    with open(rbac_path) as f:
    # Load the data from the file
        data = json.load(f)

    role_name=data["metadata"]["name"]
    namespace=data["metadata"]["namespace"]
    
    # Create the Role
    api = client.RbacAuthorizationV1Api()
    response=None
    try:
        response=api.read_namespaced_role(name=role_name, namespace=namespace)
    except client.rest.ApiException as e:
        if e.status == 404:
            print(f"The Role '{role_name}' does not exist in the '{namespace}' namespace.")
        else:
            print("Error occurred:", e)
    
    body = client.V1Role(
            metadata=client.V1ObjectMeta(name=role_name),
            rules=[
                client.V1PolicyRule(
                    api_groups=rule["apiGroups"], 
                    resources=rule["resources"], 
                    verbs=rule["verbs"]
                    )
                for rule in data["rules"]
            ],
        )
    if response!=None:
        # Update the Role object on the API server
        api.replace_namespaced_role(name=role_name, namespace=namespace, body=body)
        print("role replace done")
    else:
        api.create_namespaced_role(namespace=namespace, body=body)
        print("role creation done")
