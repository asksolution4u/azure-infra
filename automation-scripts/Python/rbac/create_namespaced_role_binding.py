from kubernetes import client, config
import json

def create_namespaced_role_binding(rbac_path):
    with open(rbac_path) as f:
    # Load the data from the file
        data = json.load(f)

    role_binding_name=data["metadata"]["name"]
    namespace=data["metadata"]["namespace"]
    role_ref=data["roleRef"]
    api = client.RbacAuthorizationV1Api()
    response=None
    try:
        response=api.read_namespaced_role_binding(name=role_binding_name, namespace=namespace)
    except client.rest.ApiException as e:
        if e.status == 404:
            print(f"The Role Binding '{role_binding_name}' does not exist in the '{namespace}' namespace.")
        else:
            print("Error occurred:", e)
    # Create the RoleBinding
    body = client.V1RoleBinding(
        metadata=client.V1ObjectMeta(name=role_binding_name),
        role_ref=client.V1RoleRef(
            kind=role_ref["kind"], 
            name=role_ref["name"], 
            api_group=role_ref["apiGroup"]
        ),
        subjects=[
            client.V1Subject(
                kind=subject["kind"], 
                name=subject["name"],
                api_group=subject["apiGroup"],
                namespace=subject["namespace"] if 'namespace' in subject else None
            )
            for subject in data["subjects"]
        ],
    )
    if response!=None:
        api.replace_namespaced_role_binding(role_binding_name,namespace,body)
    else:
        api.create_namespaced_role_binding(namespace=namespace, body=body)

