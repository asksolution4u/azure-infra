# service principal credentials for login
try {
    # get credentials from JSON file
    . .\functions.ps1
    $CREDS = Get-Creds -Config_file ".\credentials.json"
    
    # define Credentials
    $CLIENT_ID = $CREDS.CLIENT_ID
    $CLIENT_SECRET = $CREDS.CLIENT_SECRET
    $TENANT_ID = $CREDS.TENANT_ID
    $CLUSTER_RG = $CREDS.CLUSTER_RG
    $CLUSTER_NAME = $CREDS.CLUSTER_NAME
    $AD_GROUP = $CREDS.AD_GROUP
    $GROUP_NS = $CREDS.GROUP_NS
  }
  catch {
    Write-Host "ERROR: Unable to get Azure Credentials, Check your JSON configuration !"
    Exit 1
  }
  
  # clean kube config
  $homeDir = [Environment]::GetFolderPath("UserProfile")
  $kubeDir = Join-Path $homeDir ".kube"
  
  . .\functions.ps1
  Clear-KubeConfig -homeDir $homeDir -kubeDir $kubeDir
  
  # prepare secret for authentication
  $SEC_PASSWORD = ConvertTo-SecureString $CLIENT_SECRET -AsPlainText -Force
  $MY_CREDS = New-Object System.Management.Automation.PSCredential($CLIENT_ID, $SEC_PASSWORD)
  
  # connect to azure 
  Write-Host "Connecting to Azure ... `n"
  Connect-AzAccount -Credential $MY_CREDS -Tenant $TENANT_ID -ServicePrincipal
  Write-Host " Connected to Azure ! `n"
  
  # get aks cluster credentials
  Write-Host "Getting AKS Credentials ... `n"
  az aks get-credentials -g $CLUSTER_RG -n $CLUSTER_NAME --admin
  Write-Host "Credentials updated ! `n"

  $AKS_ID = $(az aks show --resource-group $CLUSTER_RG --name $CLUSTER_NAME --query id -o tsv)
  $GROUP_ID = $(az ad group show -g $AD_GROUP --query id -o tsv)

  try {
    # delete role and rolebinding
    Write-Host "deleting role and Role Binding for '$AD_GROUP' ... `n"
    kubectl delete role $($AD_GROUP + "-user-full-access") -n $GROUP_NS
    kubectl delete rolebinding $($AD_GROUP + "-user-access") -n $GROUP_NS
    Write-Host "role and Role Binding for '$AD_GROUP' Deleted Successfully ! `n"
    
    az role assignment delete --assignee $GROUP_ID --role "Azure Kubernetes Service Cluster User Role" --scope $AKS_ID

    # delete namespace
    Write-Host "deleting namespace '$GROUP_NS' ... `n"
    kubectl delete namespace $GROUP_NS
    Write-Host "namespace '$GROUP_NS' Deleted Successfully ! `n"
  }
  catch {
    Write-Host "ERROR: error occured while deleting the role !"
    Exit 1
  }

  