module "application-gateway" {
     source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/app_gateway"
 
  #source  = "../../../terraformmodules/modules/app_gateway"
  
  resource_group_name  = var.resource_group_name 
  location             = var.location 
  virtual_network_name = var.virtual_network_name
  subnet_name          = var.subnet_name 
  app_gateway_name     = var.app_gateway_name 
  azurerm_public_ip = var.azurerm_public_ip

  sku = var.sku

  backend_address_pools = var.backend_address_pools 
  backend_http_settings = var.backend_http_settings 

  http_listeners = var.http_listeners

  request_routing_rules = var.request_routing_rules 
  priority= var.priority
  tags =  var.tags 
}