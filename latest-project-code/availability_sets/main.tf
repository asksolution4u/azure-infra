module "availability_set" {
  source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/availability_set"
  resource_group_name = var.resource_group_name
  availability_set_name = var.availability_set_name
  tags = var.tags
  location = var.location
  }
