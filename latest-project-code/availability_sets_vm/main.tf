


module "availability_set_vm" {
  source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/availability_sets_vm"
  resource_group_name = var.resource_group_name
  availability_set_id = var.availability_set_id
  security_rules = var.security_rules
  subnet_name = var.subnet_name
virtual_network_name = var.virtual_network_name
location=var.location
azurerm_public_ip_name= var.azurerm_public_ip_name
azurerm_public_ip_allocation_method =var.azurerm_public_ip_allocation_method
azurerm_network_interface= var.azurerm_network_interface
ip_configuration_name =var.ip_configuration_name
private_ip_address_allocation =var.private_ip_address_allocation
azurerm_network_security_group_name =var.azurerm_network_security_group_name
azurerm_linux_virtual_machine_name =var.azurerm_linux_virtual_machine_name
azurerm_linux_virtual_machine_size =var.azurerm_linux_virtual_machine_size
admin_username=var.admin_username
admin_password =var.admin_password
source_image_reference_sku=var.source_image_reference_sku
os_disk_storage_account_type =var.os_disk_storage_account_type
os_disk_caching=var.os_disk_caching
init_script_name =var.init_script_name
  
}


