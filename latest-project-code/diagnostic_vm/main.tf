module "diagnostic" {
  source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/diagnostic-vm"
  resource_group_name = var.resource_group_name
  location = var.location
  retention_days = var.retention_days
  sku = var.sku
  metrics = var.metrics
  logs = var.logs
  diagnostics_settings_name = var.diagnostics_settings_name
  log_analytics_workspace_id = var.log_analytics_workspace_id
  virtual_machine_id = var.virtual_machine_id
  
  
}