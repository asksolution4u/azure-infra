
module "frontdoor" {
  source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/front_door"
  resource_group_name = var.resource_group_name
  location            = var.location 
  frontdoor_name      = var.frontdoor_name 
  routing_rules =var.routing_rules
  backend_pool_load_balancing = var.backend_pool_load_balancing 
  backend_pool_health_probes =var.backend_pool_health_probes 
  backend_pools = var.backend_pools
  frontend_endpoints = var.frontend_endpoints
  tags = var.tags
}



