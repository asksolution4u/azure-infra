module "private_endpoint" {
 source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/private_endpoint"
  #source = "../modules/private_endpoint"
  resource_group_name = var.resource_group_name
  private_service_connection_name = var.private_service_connection_name
  subnet_name = var.subnet_name
virtual_network_name = var.virtual_network_name
location=var.location
private_endpoint_name= var.private_endpoint_name
private_connection_resource_id = var.private_connection_resource_id
subresource_names = var.subresource_names
}