module "scaleset_app_gateway" {
 # source = "../modules/scaleset_app_gateway"
   source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/scaleset_app_gateway"
  
  resource_group_name = var.resource_group_name
  subnet_name = var.subnet_name
  virtual_network_name = var.virtual_network_name
  lb_public_ip_name = var.lb_public_ip_name
  location = var.location
  ip_allocation_method = var.ip_allocation_method
  tags = var.tags
  sku_tier = var.sku_tier
  lb_name = var.lb_name
application_port = var.application_port
   lb_rule_name                     = var.lb_rule_name
upgrade_policy_mode = var.upgrade_policy_mode 
scale_set_name = var.scale_set_name
sku_name = var.sku_name
capacity = var.capacity
 
   image_publisher = var.image_publisher 
   image_offer     = var.image_offer 
   image_sku       = var.image_sku 
   image_version   = var.image_version 
   os_caching_type           =var.os_caching_type  
   os_create_option     =var.os_create_option
   managed_disk_type =var.managed_disk_type 
   lun          = var.lun 
   data_disk_caching_type        = var.data_disk_caching_type 
   data_disk_create_option  = var.data_disk_create_option 
   data_size_gb   = var.data_size_gb 
   os_profile_computer_name = var.os_profile_computer_name 
   admin_user       = var.admin_user
   admin_password       = var.admin_password
   init_script_name =var.init_script_name
   disable_password_authentication = var.disable_password_authentication 
   network_profile_name    = var.network_profile_name 
  azurerm_monitor_autoscale_setting_name                = var.azurerm_monitor_autoscale_setting_name 
  azurerm_private_endpoint_name= var.azurerm_private_endpoint_name
azurerm_private_link_service_name = var.azurerm_private_link_service_name
private_service_connection_name= var.private_service_connection_name

}