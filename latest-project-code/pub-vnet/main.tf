module "pub-vnet" {
  source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/pub-vnet"
  #source = "../modules/pub-vnet"
  resource_group_name = var.resource_group_name
  vnet_configs        = var.vnet_configs
  subnets = var.subnets
  location = var.location
  
  
}