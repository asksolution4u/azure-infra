module "pvt-vnet" {
  source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/pvt-vnet"
  #source = "../modules/pvt-vnet"
  resource_group_name = var.resource_group_name
  vnet_configs = var.vnet_configs
  subnets = var.subnets
  nat_gateways = var.nat_gateways
  location = var.location
  
}