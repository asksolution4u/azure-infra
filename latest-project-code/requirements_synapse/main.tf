  module "keyvault" {
  source              = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/keyvault"
  resource_group_name = var.rg_name
  kvault_name         = var.keyvault_name
  disk_encryption     = var.disk_encryption_enabled
  soft_retention_days = var.retention_days
  sku                 = var.kvault_sku
  keyname_storage     = var.keyname_storage
  keysize             = var.key_size
  keytype             = var.key_type
  keyname_synapse     = var.keyname_synapse
  service_principal_object_id = var.service_principal_object_id

}
module "storage" {
  source               = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/storage"
  storage_name         = var.storage_name
  resource_group_name  = var.rg_name
  virtual_network_name = var.vnet_name
  subnet_name          = var.endpoint_subnet_name
  kvault_name          = var.keyvault_name
  kvault_rg            = var.rg_name
  keyname_storage      = var.keyname_storage
  file_system_name     = var.file_system_name

  depends_on = [module.keyvault]

}

