variable "rg_name" {
    description = "Name of the resource group in which all resources are being created"
}
variable "vnet_name" {
    description = "Name of the virtual network"
}
variable "keyvault_name" {
   description = "Keyvault name"
}
variable "disk_encryption_enabled" {
    description = "Keyvault property for disk encryption"
}
variable "retention_days" {
    description = "Keyvault retention days"
}
variable "kvault_sku" {
    description = "Keyvault sku"
}
variable "keyname_synapse" {
    description = "key created in keyvault for synapse"
}
variable "keyname_storage" {
    description = "key created in keyvault for storage"
}
variable "key_size" {
    description = "size of key created in keyvault for synapse and storage"

}
variable "key_type" {
    description = "key type that is created created in keyvault"

}
variable "service_principal_object_id"{
    description = "service principal used by terraform. Need to get access on keyvault"
}
variable "endpoint_subnet_name"{
    description = "subnet name to create endpoints"

}
variable "storage_name"{
    description = "name of the storage account"
}
variable "file_system_name" {
    description = "container name for synapse"
}