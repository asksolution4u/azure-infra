module "resource_group" {
  source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/resource-group"
  
  resource_groups =  var.resource_groups
}