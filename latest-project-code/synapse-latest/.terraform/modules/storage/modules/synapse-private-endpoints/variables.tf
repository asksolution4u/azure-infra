variable "synapse_name" {
  description = "name for the synapse workspace for which private endpoint have to be created"
  type        = string
}

variable "synapse_rg_name" {
  description = "name for the resource group for synapse_workspace"
  type        = string
}
variable "endpoints_rg" {
  description = "name for the resource group for locating endpoints for synapse_workspace"
  type        = string
}

variable "virtual_network_name" {
  description = "name for the virtual network for private endpoint"
  type        = string
}

variable "vnet_rg" {
  description = "name for the vnet resource group"
  type        = string
}

variable "subnet_name" {
  description = "name for the subnet for private endpoint"
  type        = string
}

variable "sql_endpoint" {
  description = "name for the endpoint for sql"
  type        = string
}

variable "sql_connection" {
  description = "name for the endpoint connection for sql"
  type        = string
}
variable "sql_on_demand_endpoint" {
  description = "name for the endpoint for sql on demand"
  type        = string
}

variable "sql_on_demand_connection" {
  description = "name for the endpoint connection for sql on demand"
  type        = string
}
variable "dev_endpoint" {
  description = "name for the endpoint for dev"
  type        = string
}

variable "dev_connection" {
  description = "name for the endpoint connection for dev"
  type        = string
}