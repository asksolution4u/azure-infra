data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}




resource "azurerm_virtual_network" "vnet" {
  for_each = var.vnet_configs
  name                = each.key
  location            = var.location
  resource_group_name = data.azurerm_resource_group.example.name
  address_space       = each.value.address_space
  
}

resource "azurerm_subnet" "subnet" {
  for_each = var.subnets
  name                  = each.key
  resource_group_name   = data.azurerm_resource_group.example.name
  virtual_network_name = each.value.vnet_name
  address_prefixes      = each.value.address_space
  depends_on = [
    azurerm_virtual_network.vnet
  ]
  
}


