
data "azurerm_resource_group" "example" {
  name = var.resource_group_name
}


data "azurerm_subnet" "example" {
  name                 = var.subnet_name
  virtual_network_name = var.virtual_network_name
  resource_group_name  = var.resource_group_name
}

resource "random_string" "domain" {
 length  = 6
 special = false
 upper   = false
 number  = false
}


resource "azurerm_public_ip" "lb_public_ip" {
 name                         = var.lb_public_ip_name 
 location                     = var.location
 resource_group_name = data.azurerm_resource_group.example.name 
 allocation_method            = var.ip_allocation_method 
 domain_name_label            = random_string.domain.result
 tags                         = var.tags
 sku = var.sku_tier
}

resource "azurerm_lb" "lb" {
 name                = var.lb_name 
 location            =var.location 
  resource_group_name = data.azurerm_resource_group.example.name 

 frontend_ip_configuration {
   name                 = "PublicIPAddress"
   public_ip_address_id = azurerm_public_ip.lb_public_ip.id
 }

 tags = var.tags
 sku = var.sku_tier
}

resource "azurerm_lb_backend_address_pool" "lb-backend-pool" {
 loadbalancer_id     = azurerm_lb.lb.id
 name                = "BackEndAddressPool"
}

resource "azurerm_lb_probe" "vmss" {
 loadbalancer_id     = azurerm_lb.lb.id
 name                = "http-running-probe"
 port                = var.application_port
}

resource "azurerm_lb_rule" "lbnatrule" {
   loadbalancer_id                = azurerm_lb.lb.id
   name                           = var.lb_rule_name
   protocol                       = "Tcp"
   frontend_port                  = var.application_port
   backend_port                   = var.application_port
   backend_address_pool_ids        = [azurerm_lb_backend_address_pool.lb-backend-pool.id]
   frontend_ip_configuration_name = "PublicIPAddress"
   probe_id                       = azurerm_lb_probe.vmss.id
}

resource "azurerm_virtual_machine_scale_set" "vmss" {
 name                = var.scale_set_name 
 location            =var.location 
  resource_group_name = data.azurerm_resource_group.example.name 
upgrade_policy_mode = var.upgrade_policy_mode 

 sku {
   name     = var.sku_name 
   tier     = var.sku_tier 
   capacity = var.capacity 
 }

 storage_profile_image_reference {
   publisher = var.image_publisher 
   offer     = var.image_offer 
   sku       = var.image_sku 
   version   = var.image_version 
 }

 storage_profile_os_disk {
   name              = ""
   caching           =var.os_caching_type  
   create_option     =var.os_create_option
   managed_disk_type =var.managed_disk_type 
 }

 storage_profile_data_disk {
   lun          = var.lun 
   caching        = var.data_disk_caching_type 
   create_option  = var.data_disk_create_option 
   disk_size_gb   = var.data_size_gb 
 }

 os_profile {
   computer_name_prefix = var.os_profile_computer_name 
   admin_username       = var.admin_user
   admin_password       = var.admin_password
   custom_data =filebase64(var.init_script_name)
 
 }

 os_profile_linux_config {
   disable_password_authentication = var.disable_password_authentication 
 }

 network_profile {
   name    = var.network_profile_name 
   primary = true

   ip_configuration {
     name                                   = "IPConfiguration"
     subnet_id                              = data.azurerm_subnet.example.id
     load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.lb-backend-pool.id]
     primary = true
   }
 }

 tags = var.tags
}


resource "azurerm_monitor_autoscale_setting" "example" {
  name                = var.azurerm_monitor_autoscale_setting_name 
  target_resource_id  = "${azurerm_virtual_machine_scale_set.vmss.id}"
  location            =var.location
  resource_group_name = data.azurerm_resource_group.example.name 
  profile {
    name = "autoscale-cpu"

    capacity {
      default = 2
      minimum = 2
      maximum = 90
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "${azurerm_virtual_machine_scale_set.vmss.id}"
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "GreaterThan"
        threshold          = 75
      }

      scale_action {
        direction = "Increase"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "${azurerm_virtual_machine_scale_set.vmss.id}"
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "LessThan"
        threshold          = 15
      }

      scale_action {
        direction = "Decrease"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }
  }
}




