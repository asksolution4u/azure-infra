resource "azurerm_storage_container" "synapse" {
  name               = var.file_system_name
  storage_account_name = azurerm_storage_account.storageacc.name
}