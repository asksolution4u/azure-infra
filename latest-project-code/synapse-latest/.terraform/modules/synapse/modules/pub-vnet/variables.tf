variable "resource_group_name" {
  type = string
  default = ""
  description = "resource group used for creating resources"
}

variable "location" {
  type = string
  default = "East US"
  description = "Location of resources"
}


variable "vnet_configs" {
  type = map(object({
   address_space = list(string)
    
  }))
  default = {}
}

variable "subnets" {
  type = map(object({
    vnet_name = string
    address_space = list(string)
    
  }))
  default = {}
}


