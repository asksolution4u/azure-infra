data "azurerm_resource_group" "synapse" {
  name = var.resource_group_name
}

data "azurerm_storage_account" "synapse" {
  name                = var.storage_account_name
  resource_group_name =  data.azurerm_resource_group.synapse.name
}



resource "azurerm_synapse_workspace" "synapse" {
  name                                 = var.workspace_name
  resource_group_name                  = data.azurerm_resource_group.synapse.name
  location                             = data.azurerm_resource_group.synapse.location
  storage_data_lake_gen2_filesystem_id = var.data_lake_gen2_filesystem
  sql_administrator_login              = var.sql_admin
  sql_administrator_login_password     = var.sql_password
  managed_virtual_network_enabled      = true
  public_network_access_enabled        = true
  data_exfiltration_protection_enabled = false
  managed_resource_group_name          = var.managed_resource_group
   customer_managed_key {
    key_versionless_id = data.azurerm_key_vault_key.synapse.versionless_id
    key_name           = var.synapse_key
  }

  identity {
    type = var.identity_type
  }
}
resource "azurerm_synapse_workspace_aad_admin" "synapse" {
  synapse_workspace_id = azurerm_synapse_workspace.synapse.id
  login                = var.ad_admin
  object_id            = var.admin_object
  tenant_id            = var.admin_tenant

  depends_on           = [azurerm_synapse_workspace_key.synapse]
}

resource "azurerm_role_assignment" "adls-synapse-managed-identity" {
  scope                = data.azurerm_storage_account.synapse.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = azurerm_synapse_workspace.synapse.identity[0].principal_id

  depends_on = [ azurerm_synapse_workspace.synapse ]
}
