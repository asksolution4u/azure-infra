  module "keyvault" {
  source              = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/keyvault"
  resource_group_name = var.rg_name
  kvault_name         = var.keyvault_name
  disk_encryption     = var.disk_encryption_enabled
  soft_retention_days = var.retention_days
  sku                 = var.kvault_sku
  keyname_storage     = var.keyname_storage
  keysize             = var.key_size
  keytype             = var.key_type
  keyname_synapse     = var.keyname_synapse
  service_principal_object_id = var.service_principal_object_id

}
module "storage" {
  source              = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/storage"
  storage_name        = var.storage_name
  resource_group_name  = var.rg_name
  virtual_network_name = var.vnet_name
  subnet_name          = var.endpoint_subnet_name
  kvault_name          = var.keyvault_name
  kvault_rg            = var.rg_name
  keyname_storage      = var.keyname_storage
  file_system_name     = var.file_system_name

  depends_on = [module.keyvault]

}
data "azurerm_storage_container" "syn" {
  name                 = var.file_system_name
  storage_account_name = var.storage_name
      depends_on = [module.storage]
}
module "synapse" {
  source                   = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/synapse"
  resource_group_name      = var.rg_name
  managed_resource_group   = var.managed_resource_group
  storage_account_name     = var.storage_name
  data_lake_gen2_filesystem    = replace(data.azurerm_storage_container.syn.id,"blob","dfs")
  workspace_name           = var.workspace_name
  sql_admin                = var.sql_admin
  sql_password             = var.sql_password
  identity_type            = var.identity_type
  sql_pool_name            = var.sql_pool_name
  sql_pool_sku             = var.sql_pool_sku
  create_mode              = var.create_mode
  kvault_rg                = var.rg_name
  keyvault_name            = var.keyvault_name
  synapse_key              = var.keyname_synapse
  ad_admin                 = var.ad_admin
  admin_object             = var.admin_object
  admin_tenant             = var.admin_tenant
  node_size                = var.node_size
  spark_pool_name           = var.spark_pool_name
  spark_cache_size = var.spark_cache_size
  spark_node_size  = var.spark_node_size
  max_node_count_spark  = var.max_node_count_spark
  min_node_count_spark  = var.min_node_count_spark
  autopause_delay   = var.autopause_delay
  synapse_admin_object_id = var.synapse_admin_object_id
    depends_on = [module.storage]

}
module "synapse-endpoints" {
  source            = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/synapse-private-endpoints"
  virtual_network_name     = var.vnet_name
  vnet_rg                  = var.rg_name
  subnet_name              = var.endpoint_subnet_name
  sql_endpoint             = var.sql_endpoint
  sql_connection           = var.sql_connection
  sql_on_demand_endpoint   = var.sql_on_demand_endpoint
  sql_on_demand_connection = var.sql_on_demand_connection
  dev_endpoint             = var.dev_endpoint
  dev_connection           = var.dev_connection
  synapse_name             = var.workspace_name
  synapse_rg_name          = var.rg_name
  endpoints_rg             = var.rg_name
      depends_on = [module.synapse]

}
