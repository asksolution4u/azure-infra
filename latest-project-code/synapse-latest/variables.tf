variable "rg_name" {
  description = "Name of the resource group in which all resources are being created"
}
variable "vnet_name" {
  description = "Name of the virtual network"
}
variable "keyvault_name" {
  description = "Keyvault name"
}
variable "disk_encryption_enabled" {
  description = "Keyvault property for disk encryption"
}
variable "retention_days" {
  description = "Keyvault retention days"
}
variable "kvault_sku" {
  description = "Keyvault sku"
}
variable "keyname_storage" {
  description = "key created in keyvault for storage"
}
variable "key_size" {
  description = "size of key created in keyvault for synapse and storage"

}
variable "key_type" {
  description = "key type that is created created in keyvault"
}
variable "service_principal_object_id" {
  description = "service principal used by terraform. Need to get access on keyvault"
}
variable "endpoint_subnet_name" {
  description = "subnet name to create endpoints"
}
variable "storage_name" {
  description = "name of the storage account"
}
variable "file_system_name" {
  description = "container name for synapse"
}
variable "managed_resource_group" {
  description = "Managed resource group of synapse"
}
variable "workspace_name" {
  description = "synapse workspace name"
}
variable "sql_admin" {
  description = "sql admin for synapse"
}
variable "sql_password" {
  description = "password of sql admin for synapse"
}
variable "identity_type" {
  description = "synapse identity type"
}
variable "sql_pool_name" {
  description = "Name of dedicated SQl pool"
}
variable "sql_pool_sku" {
  description = "Dedicated pool Sku"
}
variable "create_mode" {
  description = "create mode for dedicated pool"
}
variable "ad_admin" {
  description = "name of azure ad admin for synapse workspace"
}
variable "admin_object" {
  description = "object id of ad admin"
}
variable "admin_tenant" {
  description = "tenant id of ad admin"
}
variable "node_size" {
  description = "node size for Spark pool"
}
variable "spark_pool_name" {
  description = "name for Spark pool"
}
variable "spark_cache_size" {
  description = "cache size for Spark pool"
}
variable "spark_node_size" {
  description = "spark node size family for Spark pool"
}
variable "max_node_count_spark" {
  description = "maximum node for Spark pool"
}
variable "min_node_count_spark" {
  description = "minimum node for Spark pool"
}
variable "autopause_delay" {
  description = "autopause delay for Spark pool"
}
variable "keyname_synapse" {
  description = "keyname  for customer managed key for Synapse"
}
variable "synapse_admin_object_id" {
  description = "object id to be synapse administrator"
}
variable "sql_endpoint" {
  description = "Name of sql endpoint for synapse"
}
variable "sql_connection" {
  description = "Name of sql endpoint connection"
}
variable "sql_on_demand_endpoint" {
  description = "Name of sql on demand endpoint for synapse"
}
variable "sql_on_demand_connection" {
  description = "Name of sql on demand endpoint connection"
}
variable "dev_endpoint" {
  description = "Name of dev endpoint for synapse"
}
variable "dev_connection" {
  description = "Name of dev endpoint connection"
}
