terraform {
    backend "azurerm" {
        resource_group_name  = "Terraform_State"
        storage_account_name = "tfstatestaacc"
        container_name       = "synapse-private-endpoints-tfstate"
        key                  = "terraform.tfstate"
    }
}
