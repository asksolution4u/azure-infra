module "synapse-endpoints" {
  source            = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/synapse-private-endpoints"
  virtual_network_name     = var.vnet_name
  vnet_rg                  = var.rg_name
  subnet_name              = var.endpoint_subnet_name
  sql_endpoint             = var.sql_endpoint
  sql_connection           = var.sql_connection
  sql_on_demand_endpoint   = var.sql_on_demand_endpoint
  sql_on_demand_connection = var.sql_on_demand_connection
  dev_endpoint             = var.dev_endpoint
  dev_connection           = var.dev_connection
  synapse_name             = var.synapse_name
  synapse_rg_name          = var.rg_name
  endpoints_rg             = var.rg_name
}
