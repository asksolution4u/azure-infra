variable "sql_endpoint" {
    description = "Name of sql endpoint for synapse"
}
variable "sql_connection" {
    description = "Name of sql endpoint connection"
}
variable "sql_on_demand_endpoint" {
    description = "Name of sql on demand endpoint for synapse"
}
variable "sql_on_demand_connection" {
    description = "Name of sql on demand endpoint connection"
}
variable "dev_endpoint" {
    description = "Name of dev endpoint for synapse"
}
variable "dev_connection" {
    description = "Name of dev endpoint connection"
}
variable "endpoint_subnet_name"{
    description = "subnet name to create endpoints"
}
variable "rg_name" {
    description = "resource group to create synapse"
}
variable "vnet_name" {
    description = "virtual network having subnet for synapse"
}
variable "synapse_name" {
  description = "name for the synapse workspace for which private endpoint have to be created"
  type        = string
}
