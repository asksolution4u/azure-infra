data "azurerm_storage_container" "syn" {
  name                 = var.file_system_name
  storage_account_name = var.storage_name
}

module "synapse" {
  source                   = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/synapse"
  managed_resource_group   = var.managed_resource_group
  storage_account_name     = var.storage_name
  data_lake_gen2_filesystem    = replace(data.azurerm_storage_container.syn.id,"blob","dfs")
  workspace_name           = var.workspace_name
  sql_admin                = var.sql_admin
  sql_password             = var.sql_password
  identity_type            = var.identity_type
  sql_pool_name            = var.sql_pool_name
  sql_pool_sku             = var.sql_pool_sku
  create_mode              = var.create_mode
  kvault_rg                = var.rg_name
  keyvault_name            = var.keyvault_name
  synapse_key              = var.keyname_synapse
  ad_admin                 = var.ad_admin
  admin_object             = var.admin_object
  admin_tenant             = var.admin_tenant
  node_size                = var.node_size
  spark_pool_name           = var.spark_pool_name
  spark_cache_size = var.spark_cache_size
  spark_node_size  = var.spark_node_size
  max_node_count_spark  = var.max_node_count_spark
  min_node_count_spark  = var.min_node_count_spark
  autopause_delay   = var.autopause_delay
  synapse_admin_object_id = var.synapse_admin_object_id
}