variable "managed_resource_group" {
    description = "Managed resource group of synapse"
}
variable "file_system_name" {
    description = "name of container for synapse"
}
variable "storage_name"{
    description = "Name of storage account which has container for synapse"
}
variable "workspace_name" {
    description = "synapse workspace name"
}
variable "sql_admin" {
    description = "sql admin for synapse"
}
variable "sql_password" {
    description = "password of sql admin for synapse"
}
variable "identity_type" {
    description = "synapse identity type"
}
variable "sql_pool_name" {
    description = "Name of dedicated SQl pool"
}
variable "sql_pool_sku" {
    description = "Dedicated pool Sku"
}
variable "create_mode" {
    description = "create mode for dedicated pool"
}
variable "ad_admin" {
    description = "name of azure ad admin for synapse workspace"
}
variable "admin_object" {
    description = "object id of ad admin"
}
variable "admin_tenant" {
    description = "tenant id of ad admin"
}
variable "node_size" {
    description = "node size for Spark pool"
}
variable "spark_pool_name"{
    description = "name for Spark pool"
}
variable "spark_cache_size"{
    description = "cache size for Spark pool"
}
variable "spark_node_size"{
    description = "spark node size family for Spark pool"
}
variable "max_node_count_spark"{
    description = "maximum node for Spark pool"
}
variable "min_node_count_spark"{
    description = "minimum node for Spark pool"
}
variable "autopause_delay"{
    description = "autopause delay for Spark pool"
}
variable "keyname_synapse" {
    description = "keyname  for customer managed key for Synapse"
}
variable "keyvault_name" {
    description = "keyvulat name containing key for synapse"
}
variable "rg_name" {
    description = "resource group to create synapse"
}
variable "synapse_admin_object_id"{
  description = "object id to be synapse administrator"
}