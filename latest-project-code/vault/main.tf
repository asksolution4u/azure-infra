module "vault" {
 source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/vault"
  resource_group_name = var.resource_group_name
location=var.location
azurerm_key_vault_name = var.azurerm_key_vault_name
azurerm_key_vault_key_name=var.azurerm_key_vault_key_name
key_size=var.key_size
key_type=var.key_type
purge_protection_enabled=var.purge_protection_enabled
enabled_for_disk_encryption=var.enabled_for_disk_encryption
azurerm_key_vault_sku_name=var.azurerm_key_vault_sku_name
  
}


