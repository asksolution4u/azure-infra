variable "subnet_name" {
  description = "The name of the subnet."
  type        = string
}

variable "virtual_network_name" {
  description = "The name of the virtual network."
  type        = string
}

variable "resource_group_name" {
  description = "The name of the resource group."
  type        = string
}

variable "azurerm_public_ip_sku_name"{
  type = string
  default = ""
  description = "sku for public ip"
}

variable "azurerm_key_vault_name" {
  description = "The name of the Azure Key Vault instance."
  type        = string
}

variable "location" {
  description = "The location of the resource group."
  type        = string
}

variable "azurerm_public_ip_name" {
  description = "Name of public ip."
  type        = string
}

variable "azurerm_public_ip_allocation_method" {
  description = "Public ip allocation method."
  type        = string
}

variable "azurerm_network_interface" {
  description = "Name of azure network interface"
  type        = string
}

variable "ip_configuration_name" {
  description = "ip configuration name."
  type        = string
}

variable "private_ip_address_allocation" {
  description = "allocation type of private ip."
  type        = string
}




variable "azurerm_key_vault_sku_name" {
  description = "The SKU of the Azure Key Vault instance."
  type        = string
}

variable "enabled_for_disk_encryption" {
  description = "Boolean flag indicating whether or not the key vault is enabled for disk encryption."
  type        = bool
}

variable "purge_protection_enabled" {
  description = "Boolean flag indicating whether or not the key vault has purge protection enabled."
  type        = bool
}

variable "key_type" {
  description = "The type of the key vault key."
  type        = string
}

variable "key_size" {
  description = "The size of the key vault key."
  type        = number
}

variable "azurerm_key_vault_key_name" {
  description = "Name of key."
  type        = string
}


variable "azurerm_disk_encryption_set_name" {
  description = "azure disk encryption set name."
  type        = string
}

variable "azurerm_disk_encryption_set_type" {
  description = "azure disk encryption set type."
  type        = string
}

variable "azurerm_disk_encryption_set_identy_type" {
  description = "azure disk encryption set identity type."
  type        = string
}

variable "security_rules" {
  type = list(object({
    name                       = string
    priority                   = number
    description = optional(string)
    destination_address_prefixes = optional(string)
    destination_application_security_group_ids = optional(string)
    destination_port_ranges = optional(string)
    source_address_prefixes = optional(string)
    source_application_security_group_ids = optional(string)
    source_port_ranges = optional(string)
    direction                  = string
    access                     = string
    protocol                   = string
    source_port_range          = string
    destination_port_range     = string
    source_address_prefix      = string
    destination_address_prefix = string
  }))
}
  
variable "azurerm_network_security_group_name" {
  description = "The name of the network security group."
  type        = string
}

variable "os_disk_size"{
  type = number
  description = "size of os disk"
}

variable "azurerm_linux_virtual_machine_name" {
  description = "The name of the Linux virtual machine."
  type        = string
}

variable "azurerm_linux_virtual_machine_size" {
  description = "The size of the Linux virtual machine."
  type        = string
}

variable "admin_username" {
  description = "The username of the Linux virtual machine admin."
  type        = string
}

variable "admin_password" {
  description = "The password of the Linux virtual machine admin."
  type        = string
}

variable "source_image_reference_sku" {
  description = "The SKU of the source image reference."
  type        = string
}

variable "os_disk_storage_account_type" {
  description = "The storage account type of the os disk."
  type        = string
  default = ""
}
variable "os_disk_caching" {
  description = "The caching type  of the os disk."
  type        = string
  default = ""
}


variable "init_script_name"{

   description = "name of script"
  type        = string
  default = ""

}
