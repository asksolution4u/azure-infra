terraform {
    backend "azurerm" {
        resource_group_name  = "terraform-tfstate"
        storage_account_name = "asksolutiontfstate"
         container_name       = "waf-tfstate"
        key                  = "terraform.tfstate"
    }
}