module "waf" {
 source = "git::https://gitlab.com/asksolution4u/terraformmodules.git//modules/waf"
 # source = "../../../terraformmodules/modules/waf"
  resource_group_name = var.resource_group_name
  
location=var.location
managed_rules = var.managed_rules
policy_settings = var.policy_settings
custom_rules = var.custom_rules
azurerm_web_application_firewall_policy_name = var.azurerm_web_application_firewall_policy_name

}