terraform {
    backend "azurerm" {
        resource_group_name  = "aks"
        storage_account_name = "aksmain"
        container_name       = "tfstate"
        key                  = "terraform.tfstate"
    }
}
