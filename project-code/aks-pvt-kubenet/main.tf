data "azurerm_subnet" "vnet" {
  name                 = var.subnet_name
  virtual_network_name = var.virtual_network_name
  resource_group_name  = var.resource_group_name
}

module "aks" {
  source = "../../modules/aks"
  prefix                                 = var.prefix
  cluster_name                           = var.cluster_name
  resource_group_name                    = var.resource_group_name
  admin_username                         = var.admin_username
  azure_policy_enabled                   = var.azure_policy_enabled
  log_analytics_workspace_enabled        = var.log_analytics_workspace_enabled
  agents_size                            = var.agents_size
  agents_tags = {
    env = var.environment
  }
  agents_labels = {
    env = var.environment
  }
  agents_availability_zones               = var.agents_availability_zones
  agents_type                             = var.agents_type
  agents_pool_name                        = var.agents_pool_name
  enable_auto_scaling                     = var.enable_auto_scaling
  agents_min_count                        = var.agents_min_count
  agents_max_count                        = var.agents_max_count
  agents_count                            = var.agent_count
  agents_max_pods                         = var.agents_max_pods
  http_application_routing_enabled        = var.http_application_routing_enabled
  ingress_application_gateway_enabled     = var.ingress_application_gateway_enabled
  ingress_application_gateway_name        = var.ingress_application_gateway_name
  ingress_application_gateway_subnet_cidr = var.ingress_application_gateway_subnet_cidr
  os_disk_size_gb                         = var.os_disk_size_gb
  os_disk_type                            = var.os_disk_type
  network_plugin                          = var.network_plugin
  rbac_aad                                = var.rbac_aad
  rbac_aad_managed                        = var.rbac_aad_managed
  role_based_access_control_enabled       = var.role_based_access_control_enabled
  kubernetes_version                      = var.kubernetes_version
  orchestrator_version                    = var.orchestrator_version
  vnet_subnet_id                          = data.azurerm_subnet.vnet.id
  rbac_aad_admin_group_object_ids         = var.admin_group_ids
  tags = {
    environmet = var.environment
  }
}
