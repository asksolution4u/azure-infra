variable "agent_count" {
  default = 3
}

# The following two variable declarations are placeholder references.
# Set the values for these variable in terraform.tfvars
variable "aks_service_principal_app_id" {
  default = ""
}

variable "aks_service_principal_client_secret" {
  default = ""
}

variable "cluster_name" {
  default = "demo"
}

variable "dns_prefix" {
  default = "aks"
}

variable "prefix" {
  default = "aks"
}

variable "admin_username" {
  default = "ubuntu"
}

variable "azure_policy_enabled" {
  default = true
}

variable "log_analytics_workspace_enabled" {
  default = false
}

variable "agents_size" {
  default = "Standard_D2s_v3"
}

# Refer to https://azure.microsoft.com/global-infrastructure/services/?products=monitor for available Log Analytics regions.
variable "log_analytics_workspace_location" {
  default = "eastus"
}

variable "log_analytics_workspace_name" {
  default = "testLogAnalyticsWorkspaceName"
}

# Refer to https://azure.microsoft.com/pricing/details/monitor/ for Log Analytics pricing
variable "log_analytics_workspace_sku" {
  default = "PerGB2018"
}

variable "resource_group_location" {
  default     = "eastus"
  description = "Location of the resource group."
}

variable "location" {
  default     = "eastus"
  description = "Location of the resource group."
}

variable "environment" {
  default     = "dev"
  description = "environment"
}

variable "agents_availability_zones" {
  default     = ["1", "2"]
}

variable "agents_type" {
  default     = "VirtualMachineScaleSets"
}

variable "agents_pool_name" {
  default     = "aksnodepool"
}

variable "enable_auto_scaling" {
  default     = true
}

variable "agents_min_count" {
  default     = 1
}

variable "agents_max_count" {
  default     = 2
}

variable "agents_count" {
  default     = null
}

variable "agents_max_pods" {
  default     = 100
}

variable "http_application_routing_enabled" {
  default     = true
}

variable "ingress_application_gateway_enabled" {
  default     = true
}

variable "ingress_application_gateway_name" {
  default     = "aks-agw"
}

variable "ingress_application_gateway_subnet_cidr" {
  default     = "10.52.1.0/24"
}

variable "os_disk_size_gb" {
  default     = 80
}

variable "os_disk_type" {
  default     = "Managed"
}

variable "network_plugin" {
  default     = "kubenet"
}

variable "rbac_aad" {
  default     = true
}

variable "rbac_aad_managed" {
  default     = true
}

variable "role_based_access_control_enabled" {
  default     = true
}

variable "kubernetes_version" {
  default     = "1.24.9"
}

variable "orchestrator_version" {
  default     = "1.24.9"
}

variable "resource_group_name" {
  default     = "aks"
  description = "Prefix of the resource group name that's combined with a random ID so name is unique in your Azure subscription."
}

variable "ssh_public_key" {
  default = "~/.ssh/id_rsa_azure.pub"
}

variable "admin_group_ids" {

}

variable "virtual_network_name" {
  default = "aks"
}

variable "subnet_name" {
  default = "aks"
}
