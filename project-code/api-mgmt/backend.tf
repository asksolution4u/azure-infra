terraform {
    backend "azurerm" {
        resource_group_name  = "tf-resource-grp"
        storage_account_name = "apimgmt"
        container_name       = "api-tfstate"
        key                  = "terraform.tfstate"
    }
}