
data "azuread_client_config" "current" {}

resource "azurerm_api_management" "api_management" {
  name                = var.api_management_name
  location            = var.location
  resource_group_name = var.resource_group_name
  publisher_name      = var.publisher_name
  publisher_email     = var.publisher_email
  sku_name            = var.sku_name

  
}


resource "azurerm_api_management_api" "api_name" {
  name                = var.api
  resource_group_name = var.resource_group_name
  subscription_required = false
  api_management_name = azurerm_api_management.api_management.name
  revision            = var.revision
  display_name        = var.display_name
  path                = var.path
  protocols           = var.protocols
  service_url = var.service_url
  oauth2_authorization{
    authorization_server_name = azurerm_api_management_authorization_server.example.name
  }

  
}


resource "azurerm_api_management_api_policy" "example" {
  api_name            = azurerm_api_management_api.api_name.name
  api_management_name =  azurerm_api_management.api_management.name
  resource_group_name = var.resource_group_name

  xml_content = <<XML
<policies>
    <inbound>
        <base />
        
        <validate-jwt header-name="Authorization" failed-validation-httpcode="401" failed-validation-error-message="token not provided">
            <openid-config url="${var.openid_config}"/>
            <audiences>
                <audience>${azuread_application.example_backend_app.application_id}</audience>
            </audiences>
        </validate-jwt>
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
XML
}


resource "azurerm_api_management_api_operation" "example" {
  operation_id        = "user-delete"
  api_name            = azurerm_api_management_api.api_name.name
  api_management_name =  azurerm_api_management.api_management.name
  resource_group_name = var.resource_group_name
  display_name        = "GET Operation"
  method              = "GET"
  url_template        = ""
  description         = "get"

  response {
    status_code = 200
  }
}


resource "azuread_application" "example_backend_app" {
  display_name     = var.backend_app_name 
  sign_in_audience = var.sign_in_audience
  owners           = [data.azuread_client_config.current.object_id]
  identifier_uris  = ["api://example_backend_app"]
  api {
    requested_access_token_version = 2

   
    oauth2_permission_scope {
      admin_consent_description  = "Allow the application to access example on behalf of the signed-in user."
      admin_consent_display_name = "Access example"
      enabled                    = true
      id                         = var.oauth_id
      type                       =var.oauth_type 
      value                      =var.oauth_value 
    }

     }
     
     web {
    redirect_uris = ["http://localhost/"]


    
  }
  
  
}




resource "azuread_application" "example_client_app" {
  display_name     = var.client_app_name
  sign_in_audience =var.sign_in_audience 
  owners           = [data.azuread_client_config.current.object_id]

  api {
    requested_access_token_version = 2

     }

    required_resource_access {
    resource_app_id = azuread_application.example_backend_app.application_id

    resource_access {
      id   = var.oauth_id 
      type = var.resource_access_type 
    }

    
}

}
resource "azuread_application_password" "secret" {
  application_object_id = azuread_application.example_client_app.object_id
}


resource "azurerm_api_management_authorization_server" "example" {
  name                         = "test-server"
  api_management_name          = azurerm_api_management.api_management.name
  resource_group_name          = var.resource_group_name
  display_name                 = "Test Server"
  authorization_endpoint       = var.authorization_endpoint 
  client_id                    = azuread_application.example_client_app.application_id
  token_endpoint = var.token_endpoint
  client_registration_endpoint =var.client_registration_endpoint
  client_secret =  azuread_application_password.secret.value
  bearer_token_sending_methods = ["authorizationHeader"]
  client_authentication_method  = ["Body"]
  grant_types = [
    "clientCredentials",
  ]
  authorization_methods = [
    "GET",
  ]
}