
output "value" {
  description = " value"
  value       = azuread_application_password.secret.value
  sensitive = true
}

output "id" {
  description = " id"
  value       = azuread_application_password.secret.key_id
  sensitive = true
}

