
variable "location" {
  type    = string
  default = "westus2"
}

variable "resource_group_name" {
  type    = string
  default = ""
}

variable "service_url"{
  type = string
  default = ""
}

variable "publisher_name" {
  type    = string
  default = ""
}

variable "publisher_email" {
  type    = string
  default = ""
}

variable "sku_name" {
  type    = string
  default = "Developer_1"
}


variable "api_name" {
  type = string
  default = ""
}


variable "api_management_name" {
  type = string
  default = ""
}

variable "revision" {
  type = string
  default = ""
}

variable "display_name" {
  type = string
  default = ""
}

variable "path" {
  type = string
  default = ""
}

variable "protocols" {
  type = list(string)
  default = []
}

variable "swagger_link_json" {
  type = string
  default = ""
}


variable "content_format"{
  type = string
  default = ""
}


variable "openid_config"{
  type = string
  default = ""
}

variable "api"{
  type = string
  default = ""
}

variable "backend_app_name"{
  type = string
  default = ""
}

variable "client_registration_endpoint"{
  type = string
  default = ""
}


variable "sign_in_audience"{
  type = string
  default = ""
}

variable "oauth_id"{
  type = string
  default = ""
}

variable "token_endpoint"{
  type = string
  default = ""
}

variable "oauth_type"{
  type = string
  default = ""
}


variable "oauth_value"{
  type = string
  default = ""
}

variable "client_app_name"{
  type = string
  default = ""
}

variable "resource_access_type"{
  type = string
  default = ""
}

variable "authorization_endpoint"{
  type = string
  default = ""
}