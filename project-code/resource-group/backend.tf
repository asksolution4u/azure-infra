terraform {
    backend "azurerm" {
        resource_group_name  = "tf-resource-grp"
        storage_account_name = "apimgmt"
        container_name       = "resource-grp-tfstate"
        key                  = "terraform.tfstate"
    }
}