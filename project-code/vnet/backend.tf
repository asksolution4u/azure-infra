terraform {
    backend "azurerm" {
        resource_group_name  = "aks"
        storage_account_name = "aksmain"
        container_name       = "vnettfstate"
        key                  = "terraform.tfstate"
    }
}
