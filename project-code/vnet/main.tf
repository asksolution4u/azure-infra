module "vnet" {
  source  = "../../modules/vnet"
  use_for_each = false
  resource_group_name = var.resource_group_name
  vnet_location = var.resource_group_location
  address_space = var.vnet_address_space
  subnet_names = var.subnet_name
  subnet_prefixes = var.subnet_prefixes
  vnet_name = var.virtual_network_name
  tags = {
    env = var.environment
  }
}
