output "vnet_address_space" {
  sensitive = true
  value     = module.vnet.vnet_address_space
}

output "vnet_location" {
  sensitive = true
  value     = module.vnet.vnet_location
}

output "vnet_name" {
  sensitive = true
  value     = module.vnet.vnet_name
}

output "vnet_subnets" {
  sensitive = true
  value     = module.vnet.vnet_subnets
}
