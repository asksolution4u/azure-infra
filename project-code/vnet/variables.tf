variable "virtual_network_name" {
  default = "aks"
}

variable "subnet_name" {
  default = ["aks"]
}

variable "vnet_address_space" {
  default = ["10.52.0.0/16"]
}

variable "subnet_prefixes" {
  default = ["10.52.0.0/24"]
}

variable "resource_group_name" {
  default     = "aks"
  description = "Prefix of the resource group name that's combined with a random ID so name is unique in your Azure subscription."
}

variable "resource_group_location" {
  default     = "eastus"
  description = "Location of the resource group."
}

variable "environment" {
  default = "dev"  
}
