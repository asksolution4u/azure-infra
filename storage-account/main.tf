data "azurerm_storage_account" "example" {
  resource_group_name = "test"
  name = "testsa180423"
}

resource "azurerm_storage_queue" "example" {
  for_each = { for container in toset(var.queue): "${container.name}" => container if container != null }
  name                 = each.value.name
  storage_account_name = each.value.storage_account_name
}

resource "null_resource" "queue_message" {
  for_each = { for container in toset(var.queue_message): "${container.queue_name}" => container if container != null && container.message != "" }
  provisioner "local-exec" {
    command = "script/message.sh"
    environment = {
        ACCOUNT_NAME = each.value.storage_account_name
        QUEUE_NAME = each.value.queue_name
        MESSAGE    = each.value.message
     }
  }
  depends_on = [
    azurerm_storage_queue.example
  ]
}