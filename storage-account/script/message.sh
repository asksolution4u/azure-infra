#/bin/sh

az storage message put --queue-name "${QUEUE_NAME}" --content "$(echo $MESSAGE)" --account-name "${ACCOUNT_NAME}"