queue = [
  {
    name = "mysamplequeue"
    storage_account_name = "testsa180423"
  },
  {
    name = "mysamplequeue2"
    storage_account_name = "testsa180423"
  }
]

queue_message = [ 
  {
  queue_name = "mysamplequeue"
  storage_account_name = "testsa180423"
  message = <<-EOT
  [{
    app_id: 8540,
    app_full_name: Fast Analytics
  },
  {
    app_id: 1454,
    app_full_name: slow analytics
  }]
  EOT  
  },
  {
  queue_name = "mysamplequeue2"
  storage_account_name = "testsa180423"
  message = <<-EOT
  [{
    app_id: 8540,
    app_full_name: Fast Analytics
  },
  {
    app_id: 1454,
    app_full_name: slow analytics
  }]
  EOT  
  },
]