variable "queue" {
  type = list(object({
    name = string
    storage_account_name = string
  }
  ))
  description = "storage account queue"
  default = []
}

variable "queue_message" {
    type = list(object({
        queue_name = string
        message = any
        storage_account_name = string
    }))
    description = "message that needs to push to the given queue"
    default = []
}