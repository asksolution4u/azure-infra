virtual_network_name = "aks"
subnet_name = ["aks"]
resource_group_location = "eastus"
resource_group_name = "aks"
vnet_address_space = ["10.52.0.0/16"]
subnet_prefixes = ["10.52.0.0/24"]
environment= "dev"
